@extends('layouts.app')

@section('title', 'Product Details')

@section('content')
    <h1>Product Details</h1>
    <table class="table table-bordered">
        <tr>
            <th>Title</th>
            <td>{{ $product->title }}</td>
        </tr>
        <tr>
            <th>Description</th>
            <td>{{ $product->description }}</td>
        </tr>
        <tr>
            <th>Price</th>
            <td>{{ $product->price }}</td>
        </tr>
        <tr>
            <th>Stock</th>
            <td>{{ $product->stock }}</td>
        </tr>
    </table>
    <a href="{{ route('products.index') }}" class="btn btn-secondary">Back to Products</a>
@endsection
