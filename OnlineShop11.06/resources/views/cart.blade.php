@extends('layouts.app')

@section('title', 'Shopping Cart')

@section('content')
    <div class="container mt-4">
        <h1>Shopping Cart</h1>

        @if(session('cart'))
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Product</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Total</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach(session('cart') as $id => $details)
                    <tr>
                        <td>{{ $details['name'] }}</td>
                        <td>{{ $details['quantity'] }}</td>
                        <td>{{ $details['price'] }}</td>
                        <td>{{ $details['price'] * $details['quantity'] }}</td>
                        <td>
                            <form action="{{ route('cart.remove', $id) }}" method="POST" class="d-inline">
                                @csrf
                                <button type="submit" class="btn btn-danger btn-sm">Remove</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div class="d-flex justify-content-end">
                <h3>Total:
                    ${{ array_reduce(session('cart'), function($carry, $item) {
                        return $carry + ($item['price'] * $item['quantity']);
                    }, 0) }}
                </h3>
            </div>

            <div class="d-flex justify-content-end mt-3">
                <a href="{{ route('checkout') }}" class="btn btn-success">Proceed to Checkout</a>
            </div>
        @else
            <div class="alert alert-warning">
                Your cart is empty.
            </div>
        @endif
    </div>
@endsection

