<?php

use App\Http\Controllers\CartController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Auth::routes();
Route::get('/', function () {
    return view('home');
});



Route::resource('products', ProductController::class);


Route::resource('cart', CartController::class);



Route::get('/cart', [CartController::class, 'index'])->name('cart.index');


Route::post('/cart/remove/{id}', [CartController::class, 'remove'])->name('cart.remove');


//Route::get('/checkout', [CheckoutController::class, 'index'])->name('checkout');



Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
