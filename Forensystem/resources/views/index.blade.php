@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Benutzerliste</div>

                    <div class="card-body">
                        @if (Auth::user()->isAdmin())
                            <a href="{{ route('users.create') }}" class="btn btn-primary mb-3">Neuen Benutzer erstellen</a>
                        @endif

                        <table class="table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>E-Mail</th>
                                @if (Auth::user()->isAdmin())
                                    <th>Aktionen</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($users as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    @if (Auth::user()->isAdmin())
                                        <td>
                                            <a href="{{ route('users.edit', $user->id) }}" class="btn btn-sm btn-primary">Bearbeiten</a>
                                            {{-- Weitere Aktionen hier --}}
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
