<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        // Nur Admins dürfen alle Benutzer sehen


        $users = User::all();
        return view('users.index', compact('users'));
    }

    public function create()
    {
        // Nur Admins dürfen neue Benutzer erstellen


        return view('users.create');
    }

    public function store(Request $request)
    {
        // Nur Admins dürfen neue Benutzer erstellen


        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|email|unique:users',
            'password' => 'required|string|min:8',
            'birthday' => 'required|date'
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'birthday' => $request->birthday,
        ]);

        return redirect()->route('users.index')->with('success', 'Benutzer erfolgreich erstellt.');
    }

    public function edit(User $user)
    {
        // Nur Admins dürfen Benutzer bearbeiten


        return view('users.edit', compact('user'));
    }

    public function update(Request $request, User $user)
    {
        // Nur Admins dürfen Benutzer bearbeiten


        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|email|unique:users,email,' . $user->id,
        ]);

        $user->update([
            'name' => $request->name,
            'email' => $request->email,
        ]);

        return redirect()->route('users.index')->with('success', 'Benutzer erfolgreich aktualisiert.');
    }

    public function destroy(User $user)
    {
        // Nur Admins dürfen Benutzer löschen


        $user->delete();

        return redirect()->route('users.index')->with('success', 'Benutzer erfolgreich gelöscht.');
    }
}
