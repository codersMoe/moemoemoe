<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Boards extends Model
{
    use HasFactory;

    protected $fillable = ['board_name', 'board_description'];


    //one to many
    public function topics()
    {
        return $this->hasMany(Topics::class);
    }
}
