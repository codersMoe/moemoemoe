<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Produktverwaltung</title>
   @vite(['resources/css/app.css','resources/sass/app.scss','resources/js/app.css'])

{{--     immer einfügen damit es eingebunden wird--}}
</head>
<body>
<header>
    <h1>Produktverwaltung</h1>
</header>
<main>
    @yield('content')

    @if(session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif


</main>
<footer>
    <p>&copy; 2024 Coders.Rocket</p>
</footer>
</body>
</html>
