@extends('layout.default')

@section('content')
    <h1>Neues Produkt hinzufügen</h1>
    <form method="POST" action="{{ route('produkte.store') }}">
        @csrf
        <div class="form-group">
            <label for="prod_name">Name:</label>
            <input type="text" name="prod_name" id="prod_name" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="prod_beschreibung">Beschreibung:</label>
            <textarea name="prod_beschreibung" id="prod_beschreibung" class="form-control" required></textarea>
        </div>
        <div class="form-group">
            <label for="prod_preis">Preis:</label>
            <input type="number" name="prod_preis" id="prod_preis" class="form-control" step="0.01" required>
        </div>
        <div class="form-group">
            <label for="katID">Kategorie:</label>
            <select name="katID" id="katID" class="form-control" required>
                @foreach($kategorien as $kategorie)
                    <option value="{{ $kategorie->id }}">{{ $kategorie->kat_name }}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Produkt hinzufügen</button>
    </form>
@endsection
