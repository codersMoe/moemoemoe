@extends('layout.default')

@section('content')
    <h1>Produktliste</h1>
    <a href="{{ route('produkte.create') }}" class="btn btn-primary mb-2">Neues Produkt hinzufügen</a>

    <table class="table">
        <thead>
        <tr>
            <th>Name</th>
            <th>Beschreibung</th>
            <th>Preis</th>
            <th>Kategorie</th>
            <th>Aktionen</th>
        </tr>
        </thead>
        <tbody>
        @foreach($produkte as $produkt)
            <tr>
                <td>{{ $produkt->prod_name }}</td>
                <td>{{ $produkt->prod_beschreibung }}</td>
                <td>{{ $produkt->prod_preis }}</td>
                <td>{{ $produkt->kategorie->kat_name }}</td>
                <td>
                    <a href="{{ route('produkte.edit', $produkt->prodID) }}" class="btn btn-sm btn-primary">Bearbeiten</a>
                    <form action="{{ route('produkte.destroy', $produkt->prodID) }}" method="POST" style="display: inline;">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-sm btn-danger">Löschen</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
