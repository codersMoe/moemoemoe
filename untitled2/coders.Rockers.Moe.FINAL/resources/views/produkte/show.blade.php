@extends('layout.default')

@section('content')
    <h1>{{ $produkt -> prod_name }}</h1>

    <p>{{ $produkt -> prod_beschreibung }}</p>
    <p>{{ $produkt -> prod_preis }}</p>
    <p>{{ $produkt -> prod_lagerbestand }}</p>
    <p>{{ $produkt -> kategorie->kat_name }}</p>

@endsection
