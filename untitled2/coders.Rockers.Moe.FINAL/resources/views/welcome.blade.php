@extends('layout.default')

@section('content')


    <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Welcome</title>

</head>
<body>
<div class="container">
    <h1 class="mt-5">Willkommen zur Produktverwaltung</h1>
    <div class="row mt-5">
        <div class="col-md-6">
            <a href="{{ route('produkte.create') }}"><button class="btn btn-primary btn-lg btn-block">Produkt erstellen</button></a>
        </div>
        <div class="col-md-6">
            <a href="{{ route('produkte.index') }}"><button class="btn btn-success btn-lg btn-block">Produkte anzeigen</button></a>
        </div>

    </div>
</div>

</body>
</html>

@endsection
