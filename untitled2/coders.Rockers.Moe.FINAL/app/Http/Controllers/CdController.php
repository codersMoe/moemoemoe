<?php

namespace App\Http\Controllers;

use App\Models\Cd;
use Illuminate\Http\Request;

class CdController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Cd $cd)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Cd $cd)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Cd $cd)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Cd $cd)
    {
        //
    }
}
