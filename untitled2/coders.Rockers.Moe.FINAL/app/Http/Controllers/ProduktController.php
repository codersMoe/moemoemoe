<?php

namespace App\Http\Controllers;

use App\Models\Kategorie;
use Illuminate\Http\Request;
use App\Models\Produkt;

class ProduktController extends Controller
{
    public function index()
    {
        $produkte = Produkt::all();
        return view('produkte.index', compact('produkte'));
    }

    public function create()
    {
        // Zeige das Formular zum Erstellen eines neuen Produkts an
        return view('produkte.create');
    }

    public function store(Request $request)
    {
        // Validiere die Eingaben des Benutzers
        $request->validate([
            'prod_name' => 'required|string|max:255',
            'prod_beschreibung' => 'required|string',
            'prod_preis' => 'required|numeric',
            'katID' => 'required|exists:kategorien,id', // Stelle sicher, dass die Kategorie existiert
        ]);

        // Erstelle ein neues Produkt mit den übermittelten Daten
        Produkt::create([
            'prod_name' => $request->prod_name,
            'prod_beschreibung' => $request->prod_beschreibung,
            'prod_preis' => $request->prod_preis,
            'prod_lagerbestand' => $request->prod_lagerbestand ?? 0, // Falls Lagerbestand nicht übergeben wird, setze Standardwert
            'katID' => $request->katID,
        ]);

        // Zeige eine Erfolgsmeldung und leite zur Produktliste weiter
        return redirect()->route('produkte.index')->with('success', 'Produkt erfolgreich erstellt.');
    }

    public function edit($id)
    {
        $produkt = Produkt::find($id);
        $kategorien = Kategorie::all();


//dd($id,$produkt);

        return view('produkte.edit', compact('produkt', 'kategorien'));
    }
    public function show($id)
    {
        $produkt=Produkt::find($id);
        return view('produkte.show', compact('produkt' ));
    }

    public function update(Request $request, $id)
    {

        $produkt=Produkt::find($id);


        // Validiere die Eingaben des Benutzers
        $request->validate([
            'prod_name' => 'required|string|max:255',
            'prod_beschreibung' => 'required|string',
            'prod_preis' => 'required|numeric',
            'katID' => 'required|exists:kategorien,katID', // Stelle sicher, dass die Kategorie existiert
        ]);

        // Aktualisiere die Daten des Produkts mit den übermittelten Daten
        $produkt->update([
            'prod_name' => $request->prod_name,
            'prod_beschreibung' => $request->prod_beschreibung,
            'prod_preis' => $request->prod_preis,
            'prod_lagerbestand' => $request->prod_lagerbestand ?? 0, // Falls Lagerbestand nicht übergeben wird, setze Standardwert
            'katID' => $request->katID,
        ]);

        // Zeige eine Erfolgsmeldung und leite zur Produktliste weiter
        return redirect()->route('produkte.index')->with('success', 'Produkt erfolgreich aktualisiert.');
    }

    public function destroy($id)
    {
        $produkt=Produkt::find($id);
        $produkt->delete();

        // Zeige eine Erfolgsmeldung und leite zur Produktliste weiter
        return redirect()->route('produkte.index')->with('success', 'Produkt erfolgreich gelöscht.');
    }
}
