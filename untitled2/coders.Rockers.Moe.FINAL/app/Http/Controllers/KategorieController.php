<?php

namespace App\Http\Controllers;

use App\Models\Produkt;
use Illuminate\Http\Request;
use App\Models\Kategorie;

class KategorieController extends Controller
{
    public function index()
    {
        $produkte = Produkt::all();
        $kategorien = Kategorie::all();
        return view('kategorien.index', compact('kategorien', 'produkte'));
    }

    public function create()
    {
        // Zeige das Formular zum Erstellen einer neuen Kategorie an
        return view('kategorien.create');
    }

    public function store(Request $request)
    {
        // Validiere die Eingaben des Benutzers
        $request->validate([
            'kat_name' => 'required|string|max:255|unique:kategorien', // Stelle sicher, dass der Kategoriename eindeutig ist
        ]);

        // Erstelle eine neue Kategorie mit den übermittelten Daten
        Kategorie::create([
            'kat_name' => $request->kat_name,
        ]);

        // Zeige eine Erfolgsmeldung und leite zur Kategorieliste weiter
        return redirect()->route('kategorien.index')->with('success', 'Kategorie erfolgreich erstellt.');
    }

    public function edit(Kategorie $kategorie)
    {
        // Zeige das Formular zum Bearbeiten der Kategorie an
        return view('kategorien.edit', compact('kategorie'));
    }

    public function update(Request $request, Kategorie $kategorie)
    {
        // Validiere die Eingaben des Benutzers
        $request->validate([
            'kat_name' => 'required|string|max:255|unique:kategorien,kat_name,' . $kategorie->id, // Stelle sicher, dass der Kategoriename eindeutig ist, außer für die aktuelle Kategorie
        ]);

        // Aktualisiere die Daten der Kategorie mit den übermittelten Daten
        $kategorie->update([
            'kat_name' => $request->kat_name,
        ]);

        // Zeige eine Erfolgsmeldung und leite zur Kategorieliste weiter
        return redirect()->route('kategorien.index')->with('success', 'Kategorie erfolgreich aktualisiert.');
    }

    public function destroy(Kategorie $kategorie)
    {
        // Lösche die Kategorie aus der Datenbank
        $kategorie->delete();

        // Zeige eine Erfolgsmeldung und leite zur Kategorieliste weiter
        return redirect()->route('kategorien.index')->with('success', 'Kategorie erfolgreich gelöscht.');
    }
}
