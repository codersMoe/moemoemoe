<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $prodID
 */
class Produkt extends Model
{
    use HasFactory;

    protected $table = 'produkte';
    protected $primaryKey = 'prodID';

    public function kategorie()
    {
        return $this->belongsTo(Kategorie::class, 'katID', 'katID');
    }

    public function tshirts()
    {
        return $this->hasOne(Tshirt::class, 'prodID', 'prodID');
    }

    public function cds()
    {
        return $this->hasOne(Cd::class, 'prodID', 'prodID');
    }


    protected $fillable = [
        'prod_name',
        'prod_beschreibung',
        'prod_preis',
        'katID',

    ];

}
