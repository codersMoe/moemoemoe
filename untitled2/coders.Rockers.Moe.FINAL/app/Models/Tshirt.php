<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tshirt extends Model
{
    use HasFactory;
    public function produkte()
    {
        return $this->belongsTo(User::class, 'prodID', 'prodID');
    }
}
