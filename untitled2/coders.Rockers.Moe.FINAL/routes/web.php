<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProduktController;
use App\Http\Controllers\KategorieController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {

 return view('welcome');


});


Route::resource('produkte', ProduktController::class);
Route::resource('kategorien', KategorieController::class);





Route::get('/edit/{id}', [ProduktController::class,'edit'])->name('edit');


