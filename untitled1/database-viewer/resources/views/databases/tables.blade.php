@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Tables in {{ $database }}</h1>
        <ul>
            @foreach ($tables as $table)
                <li><a href="{{ url('/databases/' . $database . '/tables/' . $table->{"Tables_in_$database"} ) }}">{{ $table->{"Tables_in_$database"} }}</a></li>
            @endforeach
        </ul>
    </div>
@endsection

