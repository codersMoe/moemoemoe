
{{--@extends('layouts.app')--}}

{{--@section('content')--}}
{{--    <div class="container">--}}
{{--        <h1>Databases</h1>--}}
{{--        <ul>--}}
{{--            @foreach ($databases as $database)--}}
{{--                <li><a href="{{ url('/databases/' . $database->Database . '/tables') }}">{{ $database->Database }}</a></li>--}}
{{--            @endforeach--}}
{{--        </ul>--}}
{{--    </div>--}}
{{--@endsection--}}
    <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Datenbankverwaltung</title>
</head>
<body>
<h1>Datenbankverwaltung</h1>

<div style="display: flex;">
    <!-- Linke Spalte für Datenbanken -->
    <div style="flex: 1;">
        <h2>Datenbanken</h2>
        <ul>
            @foreach ($databases as $database)
                            <li><a href="{{ url('/databases/' . $database->Database . '/tables') }}">{{ $database->Database }}</a></li>
                        @endforeach
        </ul>
    </div>

    <!-- Rechte Spalte für Tabellen und Tabellendaten -->
    <div style="flex: 2;">
        @if (isset($tables))
            <div class="container">
                <h1>Tables in {{ $database }}</h1>
                <ul>
                    @foreach ($tables as $table)
                        <li><a href="{{ url('/databases/' . $database . '/tables/' . $table->{"Tables_in_$database"} ) }}">{{ $table->{"Tables_in_$database"} }}</a></li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if (isset($tableData))
            <h2>Daten in {{ $table }}</h2>
                <div class="container">
                    <h1>Data in {{ $table }} table of {{ $database }} database</h1>
                    <table class="table">
                        <thead>
                        <tr>
                            @foreach ($columns as $column)
                                <th>{{ $column }}</th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($data as $row)
                            <tr>
                                @foreach ($columns as $column)
                                    <td>{{ $row->$column }}</td>
                                @endforeach
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
        @endif
    </div>
</div>
</body>
</html>
