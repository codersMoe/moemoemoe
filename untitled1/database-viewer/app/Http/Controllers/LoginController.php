<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $credentials = $request->only('username', 'password');
        if (
            auth()->attempt($credentials)
        ) {
            Session::put('authenticated', true);
            return redirect()->route('db')->with('Succes', 'Login done');
        }
//        if ($credentials['username'] === 'admin' && $credentials['password'] === 'password') {

//            return redirect('/databases');
//        }

        return redirect('/login')->withErrors(['Invalid credentials']);
    }

    public function logout(Request $request)
    {
        Session::forget('authenticated');
        return redirect('/login');
    }
}
