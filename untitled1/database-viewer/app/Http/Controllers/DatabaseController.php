<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class DatabaseController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!Session::has('authenticated')) {
                return redirect('/login');
            }
            return $next($request);
        });
    }

    public function index()
    {

        $databases = DB::select('SHOW DATABASES');
        return view('databases.index', compact('databases'));
    }

    public function showTables($database)
    {
        DB::statement("USE $database");
        $tables = DB::select('SHOW TABLES');
        return view('databases.tables', compact('database', 'tables'));
    }

    public function showTableData($database, $table)
    {
        DB::statement("USE $database");
        $data = DB::table($table)->get();
        $columns = DB::getSchemaBuilder()->getColumnListing($table);
        return view('databases.table-data', compact('database', 'table', 'data', 'columns'));
    }




//    public function home($selectedDatabase = null, $selectedTable = null)
//    {
//
//        $dbConnection = DB::connection('mydatabase');
//
//        $databases = $dbConnection->select('SHOW DATABASES');
//        $tables = $columns = $data = null;
//
//        if($selectedDatabase){
//            $tables = $this->getTables($selectedDatabase);
//            if($selectedTable){
//                $columns = $this->getColumns($selectedDatabase,$selectedTable);
//                $data = $this->getData($selectedDatabase,$selectedTable);
//            }
//        }
//        $tables = $selectedDatabase != null ? $this->getTables($selectedDatabase) : null;
//
//        return view('home', compact('databases','selectedDatabase','tables','selectedTable','columns', 'data'));
//    }
//
//    private function getTables($database)
//    {
//        $dbConnection = $this->getDatabaseConnection($database);
//        $tables = $dbConnection->select('SHOW TABLES');
//        $tableKey = "Tables_in_$database";
//
//        return array_map(function ($table) use ($tableKey){
//            return $table->$tableKey;
//        },$tables);
//
//
//    }
//
//
//
//    private function getDatabaseConnection($database)
//    {
//        // Temporäre Verbindung ohne Session-Management erstellen
//        config(['database.connections.temp' => array_merge(
//            config('database.connections.mysql'), ['database' => $database]
//        )]);
//
//        // Temporäre Verbindung nutzen
//        return DB::connection('temp');
//    }
//    private function getColumns($database, $table)
//    {
//        $dbConnection = $this->getDatabaseConnection($database);
//
//        // Temporäre Verbindung nutzen
//        $columns = $dbConnection->select("SHOW COLUMNS FROM $table");
//        return array_map(function ($column) {
//            return $column->Field;
//        }, $columns);
//    }
//
//    private function getData($database, $table)
//    {
//        $dbConnection = $this->getDatabaseConnection($database);
//        return $dbConnection->table($table)->get();
//    }
}
