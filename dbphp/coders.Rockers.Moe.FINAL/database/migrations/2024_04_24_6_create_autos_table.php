<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('autos', function (Blueprint $table) {


            $table->string('FIN',17)->primary();


            $table->decimal('preis',10,2);

            $table->foreignId('hersteller_id')->constrained('hersteller','id')->cascadeOnDelete();
            $table->foreignId('modell_id')->constrained('modells','id')->cascadeOnDelete();
            $table->foreignId('autohaus_id')->constrained('autohaus','id')->cascadeOnDelete();
//            $table->primary('FIN');


        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('autos');
    }
};
