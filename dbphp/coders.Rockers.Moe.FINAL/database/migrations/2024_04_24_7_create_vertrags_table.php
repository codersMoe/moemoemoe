<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('vertrag', function (Blueprint $table) {
            $table->string('auto_id');
            $table->foreign('auto_id')->references('FIN')->on('autos');



            $table->foreignId('kunde_id')->constrained('kunden');

            $table->date('von');
            $table->date('bis');


        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('vertrag');
    }
};
