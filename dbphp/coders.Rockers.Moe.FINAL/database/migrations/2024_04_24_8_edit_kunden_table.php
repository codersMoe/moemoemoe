<?php

use App\Models\Stadt;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('kunden', function (Blueprint $table) {

            $table->foreignIdFor(Stadt::class)->constrained('stadt');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('kunden', function (Blueprint $table) {

            $table->dropForeign(['stadt_id']);
            $table->dropColumn('stadt_id');
        });
    }
};
