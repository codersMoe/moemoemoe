<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Autohaus extends Model
{
    use HasFactory;

    protected $table = 'autohaus';

    public function stadt()
    {
        return $this->belongsTo(Stadt::class);
    }

    public function autos()
    {
        return $this->hasMany(Auto::class);
    }
}
