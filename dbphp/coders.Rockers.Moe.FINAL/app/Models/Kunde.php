<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kunde extends Model
{
    use HasFactory;

    public function stadt()
    {
        return $this->belongsTo(Stadt::class);
    }

    public function vertrage()
    {
        return $this->belongsToMany(Auto::class)
            ->withPivot('von','bis');
    }
}
