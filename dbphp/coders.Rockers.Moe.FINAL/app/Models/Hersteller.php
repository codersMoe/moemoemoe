<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hersteller extends Model
{
    use HasFactory;

    protected $table = 'hersteller';


    protected $fillable = ['id', 'name'];

    public function autos()
    {
        return $this->hasMany(Auto::class);
    }

    public $timestamps = false;
}
