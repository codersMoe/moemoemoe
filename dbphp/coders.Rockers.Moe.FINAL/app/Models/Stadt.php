<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stadt extends Model
{
    use HasFactory;

    public function autohaus()
    {
        return $this->hasMany(Autohaus::class);
    }

    public function kunde()
    {
        return $this->hasMany(Kunde::class);
    }
}
