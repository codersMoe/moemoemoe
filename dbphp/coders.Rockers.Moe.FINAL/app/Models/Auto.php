<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use PhpParser\Node\Scalar\String_;

class Auto extends Model
{
    use HasFactory;

    public function hersteller()
    {
        return $this->belongsTo(Hersteller::class);
    }
    public function modell()
    {
        return $this->belongsTo(Modell::class);
    }
    public function autohaus()
    {
        return $this->belongsTo(Autohaus::class);
    }

    protected $primaryKey = 'FIN';
//    protected $keyType = 'String';


    protected $fillable = [
        'FIN',
        'hersteller_id',
        'modell_id',
        'autohaus_id',
        'preis',
    ];
}
