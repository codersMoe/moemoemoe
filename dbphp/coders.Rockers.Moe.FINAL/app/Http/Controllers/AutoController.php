<?php

namespace App\Http\Controllers;

use App\Models\Auto;
use App\Models\Autohaus;
use Illuminate\Http\Request;

class AutoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $autos = Auto::all();
        return view('auto.index',compact('autos'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $autohauser = Autohaus::all();
        return view('auto.create',compact('autohauser'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'FIN'=> 'required|numeric',
            'preis'=> 'required|numeric',
            'hersteller_id'=> 'required|numeric',
            'modell_id'=> 'required|numeric',
            'autohaus_id'=> 'required|numeric'

        ]);



    $auto = Auto::create([
        'FIN' => $request->FIN,
        'preis' => $request->preis,
        'hersteller_id' => $request->hersteller_id,
        'modell_id' => $request->modell_id,
        'autohaus_id' => $request->autohaus_id

    ]);

        return redirect('/autos');
    }

    /**
     * Display the specified resource.
     */
    public function show(Auto $auto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Auto $auto)
    {
        return view('auto.edit', compact('auto'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Auto $auto)
    {



        // Validiere die Eingaben des Benutzers
        $request->validate([
            'FIN'=> 'required|numeric',
            'preis'=> 'required|numeric',
            'hersteller_id'=> 'required|numeric',
            'modell_id'=> 'required|numeric',
            'autohaus_id'=> 'required|numeric'
        ]);

        // Aktualisiere die Daten des Produkts mit den übermittelten Daten
        Auto::where('FIN',$auto->FIN)->   update([
            'FIN' => $request->FIN,
            'preis' => $request->preis,
            'hersteller_id' => $request->hersteller_id,
            'modell_id' => $request->modell_id,
            'autohaus_id' => $request->autohaus_id
        ]);

        // Zeige eine Erfolgsmeldung und leite zur Produktliste weiter
        return redirect()->route('autos.index')->with('success', 'Auto erfolgreich aktualisiert.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Auto $auto)
    {
        Auto::find($auto->FIN)-> delete();

        return redirect()->route('autos.index')->with('success', 'Auto erfolgreich gelöscht.');

    }
}
