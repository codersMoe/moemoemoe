<?php

namespace App\Http\Controllers;

use App\Models\Modell;
use Illuminate\Http\Request;

class ModellController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Modell $modell)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Modell $modell)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Modell $modell)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Modell $modell)
    {
        //
    }
}
