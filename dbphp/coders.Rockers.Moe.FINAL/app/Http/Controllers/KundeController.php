<?php

namespace App\Http\Controllers;

use App\Models\Kunde;
use Illuminate\Http\Request;

class KundeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Kunde $kunde)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Kunde $kunde)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Kunde $kunde)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Kunde $kunde)
    {
        //
    }
}
