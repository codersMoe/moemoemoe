<?php

namespace App\Http\Controllers;

use App\Models\Vertrag;
use Illuminate\Http\Request;

class VertragController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Vertrag $vertrag)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Vertrag $vertrag)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Vertrag $vertrag)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Vertrag $vertrag)
    {
        //
    }
}
