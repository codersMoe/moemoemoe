<?php

namespace App\Http\Controllers;

use App\Models\Autohaus;
use App\Models\Hersteller;
use Illuminate\Http\Request;

class HerstellerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $hersteller = Hersteller::all();
        return view('hersteller.index',compact('hersteller'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $hersteller = Hersteller::all();
        $autohauser = Autohaus::all();
        return view('hersteller.create',compact('hersteller','autohauser'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'id'=> 'required|numeric',
            'name'=> 'required|alpha_num',


        ]);



        $hersteller = Hersteller::create([
            'id' => $request->id,
            'name' => $request->name


        ]);

        return view('hersteller.show', compact('hersteller'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Hersteller $hersteller)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Hersteller $hersteller)
    {
        return view('hersteller.edit', compact('hersteller'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Hersteller $hersteller)
    {



        // Validiere die Eingaben des Benutzers
        $request->validate([
            'id'=> 'required|numeric',
            'name'=> 'required|alpha_num',
        ]);

        // Aktualisiere die Daten des Produkts mit den übermittelten Daten
        Hersteller::where('id',$hersteller->id)->   update([
            'id' => $request->id,
            'name' => $request->name,
        ]);

        // Zeige eine Erfolgsmeldung und leite zur Produktliste weiter
        return redirect()->route('hersteller.index')->with('success', 'Hersteller erfolgreich aktualisiert.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Hersteller $hersteller)
    {
        $hersteller = Hersteller::find($hersteller->id);
        $hersteller -> delete();

        return redirect()->route('hersteller.index')->with('success', 'Hersteller erfolgreich gelöscht.');

    }
}
