<?php

namespace App\Http\Controllers;

use App\Models\Stadt;
use Illuminate\Http\Request;

class StadtController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Stadt $stadt)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Stadt $stadt)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Stadt $stadt)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Stadt $stadt)
    {
        //
    }
}
