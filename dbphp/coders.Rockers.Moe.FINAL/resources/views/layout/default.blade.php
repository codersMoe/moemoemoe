<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Autoverwaltung</title>
   @vite(['resources/css/app.css','resources/sass/app.scss','resources/js/app.css'])


</head>
<body>
<header>
    <h1>Autoverwaltung</h1>
</header>
<main>
    @yield('content')

    @if(session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif


</main>
<footer>
    <p>&copy; 2024 Auto.Verwaltung.Moe DARWISH</p>
</footer>
</body>
</html>
