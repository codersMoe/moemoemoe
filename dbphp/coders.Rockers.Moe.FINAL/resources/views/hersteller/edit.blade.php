@extends('layout.default')

@section('content')

    <h1>Hersteller bearbeiten</h1>

    <form method="POST" action="{{ route('hersteller.update', $hersteller) }}">

        @csrf

        @method('PUT')

        <div class="form-group">

            <label for="id">id:</label>

            <input type="number" name="id" id="id" class="form-control" value="{{ $hersteller->id }}" required readonly>

        </div>

        <div class="form-group">

            <label for="name">NAME:</label>

            <input type="alpha_num" name="name" id="name" class="form-control" value="{{ $hersteller->name  }}" required>

        </div>




        <button type="submit" class="btn btn-primary">Hersteller aktualisieren</button>

    </form>

@endsection
