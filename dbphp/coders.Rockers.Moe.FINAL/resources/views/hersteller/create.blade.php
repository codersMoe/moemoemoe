@extends('layout.default')

@section('content')
    <h1>Neuen Hersteller hinzufügen</h1>
    <form method="POST" action="/hersteller">
        @csrf
        <div class="form-group">
            <label for="id">ID:</label>
            <input type="number" name="id" id="id" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" name="name" id="name" class="form-control" required>
        </div>

                @foreach($hersteller as $hersteller)
                    <option value="{{ $hersteller->id }}">{{ $hersteller->name }}</option>
                @endforeach


        <button type="submit" class="btn btn-primary">Hersteller hinzufügen</button>
    </form>
@endsection
