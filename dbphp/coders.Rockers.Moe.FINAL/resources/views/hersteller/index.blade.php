@extends('layout.default')

@section('content')
    <h1>Autoliste</h1>
    <a href="{{ route('autos.create') }}" class="btn btn-primary mb-2">Neues Auto hinzufügen</a>
    <a href="{{ route('hersteller.create') }}" class="btn btn-primary mb-2">Neuer Hersteller hinzufügen</a>
    <a href="{{ route('modells.create') }}" class="btn btn-primary mb-2">Neues Modell hinzufügen</a>

    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Aktionen</th>

        </tr>
        </thead>
        <tbody>

        @foreach($hersteller as $hersteller)
            <tr>
                <td>{{ $hersteller->id }}</td>
                <td>{{ $hersteller->name }}</td>

                <td>

                    <a href="{{ route('hersteller.edit', $hersteller) }}" class="btn btn-warning">Bearbeiten</a>


                    <form action="{{ route('hersteller.destroy', $hersteller) }}" method="POST" style="display: inline;">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger" onclick="return confirm('Sind Sie sicher?')">Löschen</button>
                    </form>
                </td>

            </tr>
        @endforeach
        </tbody>
    </table>

@endsection
