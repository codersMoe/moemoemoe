@extends('layout.default')

@section('content')
    <h1>Neues Auto hinzufügen</h1>
    <form method="POST" action="/autos">
        @csrf
        <div class="form-group">
            <label for="FIN">FIN:</label>
            <input type="number" name="FIN" id="FIN" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="hersteller_id">Hersteller:</label>
            <input type="number" name="hersteller_id" id="hersteller_id" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="model_id">Modell</label>
            <input type="number" name="modell_id" id="modell_id" class="form-control"  required>
        </div>

        <div class="form-group">
            <label for="preis">Preis</label>
            <input type="number" name="preis" id="preis" class="form-control"  required>
        </div>
        <div class="form-group">
            <label for="autohaus_id">Autohaus</label>
            <select name="autohaus_id" id="autohaus_id" class="form-control" required>
                @foreach($autohauser as $autohaus)
                    <option value="{{ $autohaus->id }}">{{ $autohaus->name }}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Auto hinzufügen</button>
    </form>
@endsection
