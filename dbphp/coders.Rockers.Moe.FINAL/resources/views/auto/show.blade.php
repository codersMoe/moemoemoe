@extends('layout.default')

@section('content')
    <h1>FIN : {{ $auto -> FIN }}</h1>

    <p>Hersteller : {{ $auto -> hersteller -> name}}</p>
    <p>Modell  : {{ $auto -> modell -> name }}</p>
    <p>Preis  : {{ $auto -> preis }}</p>
    <p>Autohaus : {{ $auto -> autohaus -> name }}</p>

@endsection
