@extends('layout.default')

@section('content')

    <h1>Auto bearbeiten</h1>

    <form method="POST" action="{{ route('autos.update', $auto) }}">

        @csrf

        @method('PUT')

        <div class="form-group">

            <label for="FIN">FIN:</label>

            <input type="number" name="FIN" id="FIN" class="form-control" value="{{ $auto->FIN }}" required>

        </div>

        <div class="form-group">

            <label for="Hersteller">Hersteller:</label>

            <input type="number" name="hersteller_id" id="hersteller" class="form-control" value="{{ $auto->hersteller_id  }}" required>

        </div>

        <div class="form-group">

            <label for="Modell">Modell:</label>

            <input type="number" name="modell_id" id="modell" class="form-control"  value="{{ $auto->modell -> id}}" required>

        </div>

        <div class="form-group">

            <label for="autohaus">Autohaus:</label>

            <input type="number" name="autohaus_id" id="autohaus" class="form-control"  value="{{ $auto->autohaus-> id }}" required>
        </div>
            <div class="form-group">

                <label for="Preis">Preis:</label>

                <input type="number" step=".01" name="preis" id="preis" class="form-control"  value="{{ $auto->preis }}" required>

            </div>


        <button type="submit" class="btn btn-primary">Auto aktualisieren</button>

    </form>

@endsection
