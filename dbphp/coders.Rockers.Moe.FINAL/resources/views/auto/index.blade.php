@extends('layout.default')

@section('content')
    <h1>Autoliste</h1>
    <a href="{{ route('autos.create') }}" class="btn btn-primary mb-2">Neues Auto hinzufügen</a>
    <a href="{{ route('hersteller.create') }}" class="btn btn-primary mb-2">Neuer Hersteller hinzufügen</a>
    <a href="{{ route('modells.create') }}" class="btn btn-primary mb-2">Neues Modell hinzufügen</a>

    <table class="table">
        <thead>
        <tr>
            <th>FIN</th>
            <th>Hersteller</th>
            <th>Modell</th>
            <th>Autohaus</th>
            <th>Preis</th>
            <th>Aktionen</th>

        </tr>
        </thead>
        <tbody>

        @foreach($autos as $auto)
            <tr>
                <td>{{ $auto->FIN }}</td>
                <td>{{ $auto->hersteller->name }}</td>
                <td>{{ $auto->modell->name }}</td>
                <td>{{ $auto->autohaus->name }}</td>
                <td>{{ $auto->preis }}</td>
                <td>

                    <a href="{{ route('autos.edit', $auto) }}" class="btn btn-warning">Bearbeiten</a>


                    <form action="{{ route('autos.destroy', $auto) }}" method="POST" style="display: inline;">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger" onclick="return confirm('Sind Sie sicher?')">Löschen</button>
                    </form>
                </td>

            </tr>
        @endforeach
        </tbody>
    </table>

@endsection
