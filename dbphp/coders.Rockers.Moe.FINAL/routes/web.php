<?php

use App\Http\Controllers\AutoController;
use App\Http\Controllers\HerstellerController;
use App\Http\Controllers\ModellController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::resource('/autos', AutoController::class);
Route::resource('/hersteller', HerstellerController::class);
Route::resource('/modells', ModellController::class);


